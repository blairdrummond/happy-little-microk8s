
APPS := apps
TEMPLATES := templates

TO_BUILD=$(wildcard $(APPS)/*/*/Makefile)

.PHONY: namespaces build push

# Create yaml for each namespace
namespaces: $(TEMPLATES)/namespace.yaml _yaml $(APPS)/*
	@for namespace in $(APPS)/*; do \
		NAMESPACE=$$(basename $${namespace}); \
		[ -d $${namespace} ] && \
		yq -y --arg NAME $${NAMESPACE} '.metadata.name |= $$NAME' $< \
		| tee _yaml/namespace-$${NAMESPACE}.yaml; \
	done

build: $(TO_BUILD)
	for makefile in $(TO_BUILD); do \
		( cd $$(dirname $${makefile}) && make build ) ; \
	done

push: build $(TO_BUILD)
	for makefile in $(TO_BUILD); do \
		( cd $$(dirname $${makefile}) && make push ) ; \
	done
