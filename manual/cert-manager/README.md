Steps
=====

Create the custom-resource-definitions
--------------------------------------

``` sh
# First
kubectl apply -f cert-manager-arm.yaml

```

Edit the cluster issuer
-----------------------

```sh
# First edit this to use your email and stuff
kubectl apply -f letsencrypt-issuer-prod.yaml
```

