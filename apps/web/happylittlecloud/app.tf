variable "namespace" {
  description = "The target namespace"
  default     = "default"
  type        = string
}

variable "image" {
  description = "Image Location"
  default     = "blairdrummond/happylittlecloud:latest"
  type        = string
}

variable "ingress" {
  description = "Ingress url"
  default     = "happylittlecloud.ca"
  type        = string
}

variable "tls" {
  description = "Is TLS enabled?"
  type        = bool
}

resource "helm_release" "landingpage" {
  name       = "langingpage"
  chart      = "${path.module}/chart"
  namespace = var.namespace

  set {
    name = "image.image"
    value = var.image
  }

  set {
    name = "host"
    value = var.ingress
  }

  set {
    name = "tls"
    value = var.tls
  }
}
