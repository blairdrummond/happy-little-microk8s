variable "namespace" {
  description = "The target namespace"
  default     = "default"
  type        = string
}

variable "ingress" {
  description = "Ingress url"
  default     = "datasette.happylittlecloud.ca"
  type        = string
}

variable "s3_endpoint" {
  description = "URL to the s3 system storing dbs"
  default     = "https://minio.happylittlecloud.ca"
  type        = string
}

variable "s3_path" {
  description = "bucket/prefix for the content"
  default     = "www/datasette"
  type        = string
}

resource "helm_release" "music" {
  name       = "music"
  chart      = "${path.module}/chart"
  namespace  = var.namespace
  values         = [
    <<-EOF
    image:
      repository: blairdrummond/datasette
      pullPolicy: Always
      # Overrides the image tag whose default is the chart appVersion.
      tag: "0.56"

    s3:
      endpoint: ${var.s3_endpoint}
      path: ${var.s3_path}

    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: "traefik"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        traefik.ingress.kubernetes.io/frontend-entry-points: http,https
        traefik.ingress.kubernetes.io/redirect-entry-point: https
        traefik.ingress.kubernetes.io/redirect-permanent: "true"
      hosts:
      - host: ${var.ingress}
        paths:
        - path: /
      - host: www.${var.ingress}
        paths:
        - path: /
      tls:
        - hosts:
          - ${var.ingress}
          - www.${var.ingress}
          secretName: "music-cert"
    EOF
  ]
}
