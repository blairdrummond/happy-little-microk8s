#!/bin/python3

# Convert an mpd.db file into jsonlines.
# Example usage:
#
# > parse-mpd.py /nfs/music | sqlite-utils insert --alter --nl music mpd.sqlite3 music -
#
# Based on: https://github.com/Delapouite/mpdump
#
# NOTE: If your Artist, Album, or Track names start/end with " ",
#       then expect the script to fail. (This is fixable, but probably better to just
#       clean up the file names)

import gzip
import json
from os.path import join, exists

import argparse

parser = argparse.ArgumentParser(description='Jsonify an mpd database.')
parser.add_argument('music_folder', type=str, help='Root Directory of music library')
parser.add_argument('database',     type=str, help='Mpd database location')
args = parser.parse_args()

with gzip.open(args.database, "rb") as f:
    # bytes -> str
    lines = (
        x.decode('utf-8').strip()
        for x in f.readlines()
    )

def split(line):
    blocks = line.strip().split(': ')
    assert len(blocks) > 1
    return blocks[0], ': '.join(blocks[1:])


def consume(stream, acc={}):
    head = next(stream)
    if head == 'song_end':
        return acc
    else:
        k, v = split(head)
        acc[k] = v
        return consume(stream, acc)

directory = []
try:
    while True:
        line = next(lines)
        if line.startswith('directory:'):
            _, d = split(line)
            directory.append(d)
        elif directory and line.startswith('end: ' + join(*directory)):
            directory.pop()
        elif line.startswith('song_begin:'):
            _, track = split(line)
            song = consume(lines, acc={'File': join(args.music_folder, *(directory+[track]))})
            if len(directory) >= 1:
                song['FileArtist'] = directory[0]
            if len(directory) >= 2:
                song['FileAlbum'] = directory[1]
            try:
                assert exists(song['File'])
            except:
                print("~%s~" % song['File'])
                raise FileNotFoundError()
            print(json.dumps(song))

except StopIteration:
    pass
