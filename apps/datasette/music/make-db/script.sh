#!/bin/sh

python3 parse-mpd.py /nfs/music $HOME/.mpd/mpd.db | 
	jq -c 'with_entries(select( .key | test("^MUSICB*") | not))' | 
	sqlite-utils insert --alter --nl music.sqlite3 music -

gzip -9 -k music.sqlite3
