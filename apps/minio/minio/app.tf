variable "namespace" {
  description = "The target namespace"
  default     = "default"
  type        = string
}

variable "access_key" {
  description = "s3 access key"
  type        = string
}

variable "secret_key" {
  description = "s3 secret key"
  type        = string
}

variable "ingress" {
  description = "Ingress url"
  default     = "minio.happylittlecloud.ca"
  type        = string
}

variable "nfs_ip" {
  description = "nfs ip address"
  type        = string
}

variable "nfs_path" {
  description = "nfs path (e.g. /folder)"
  type        = string
}

resource "helm_release" "minio" {
  name       = "minio"
  chart      = "${path.module}/chart"
  namespace  = var.namespace
  values         = [
    <<-EOF
    image:
      repository: tobi312/minio
      pullPolicy: IfNotPresent
      # Overrides the image tag whose default is the chart appVersion.
      tag: latest

    minio:
      url: ${var.ingress}
      access_key: ${var.access_key}
      secret_key: ${var.secret_key}

    volume:
      server: ${var.nfs_ip}
      path: ${var.nfs_path}

    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: "traefik"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        traefik.ingress.kubernetes.io/frontend-entry-points: http,https
        traefik.ingress.kubernetes.io/redirect-entry-point: https
        traefik.ingress.kubernetes.io/redirect-permanent: "true"
      hosts:
      - host: ${var.ingress}
        paths:
        - path: /
      - host: www.${var.ingress}
        paths:
        - path: /
      tls:
        - hosts:
          - ${var.ingress}
          - www.${var.ingress}
          secretName: "minio-cert"
    EOF
  ]
}
