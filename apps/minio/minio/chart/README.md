# Minimal MinIO setup

The default MinIO Helm chart (or operator) are way overkill for testing.

This is a tiny little deployment to just test the functionality.
