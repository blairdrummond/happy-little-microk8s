# NOTE: The setup of NFS was done manually

**Note: I originally started this with microk8s, but moved to k3s.**

# Quick Start

0. Install `k3s` on an Arm board, and copy the `~/.kube/config` file into `.kube/config` in this directory.

1. First, run through the `manual` folder to get the lets-encrypt stuff set up. You have to do this first.

2. Go through the apps/charts and make sure the hostnames and docker urls look good.

3. Run `make build` to build *all* docker images.

4. Run `make push` to push *all* docker images.

5. Run `make tf` to create all terraform code (also creates the yaml)

6. Go into the terraform folder and run `terraform plan`

7. If it looks good, run `terraform apply`


# Docker buildx

Note, since we're pushing these with the intention of running on an arm device, need to push both x86 and arm versions. Still learning about this, but there is a tutorial here:

https://github.com/docker/buildx#binary-release

# k3s notes

Once you get `k3s` setup on the cluster, you want to copy the `~/.kube/config` into *this repo* under `./.kube/config`. This will let terraform work.

## Requirements

On the cluster

``` sh
k3s
```

On the client

```
kubectl
k2tf
helm
yq (via pip)
jq
terraform
```


# apps

Structure is `apps/$namespace/$application`. It's assumed that there are Makefiles in there.


