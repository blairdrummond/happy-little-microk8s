variable "minio_access_key" {
  description = "s3 access key"
  type        = string
}

variable "minio_secret_key" {
  description = "s3 secret key"
  type        = string
}



terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    helm = {
      source = "hashicorp/helm"
    }
  }
}

provider "kubernetes" {
  config_path = ".kube/config"
}


provider "helm" {
  kubernetes {
    config_path = ".kube/config"
  }
}


resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"

  set {
    name = "installCRDs"
    value = true
  }
}



resource "kubernetes_namespace" "web_namespace" {
  metadata {
    name = "web"
  }
}

module "bdrummond" {
  source = "./apps/web/bdrummond"
  namespace = "web"
  ingress = "blair.happylittlecloud.ca"
  tls = true
}


module "landingpage" {
  source = "./apps/web/happylittlecloud"
  namespace = "web"
  ingress = "happylittlecloud.ca"
  tls = true
}




resource "kubernetes_namespace" "minio_namespace" {
  metadata {
    name = "minio"
  }
}



module "minio" {
  source = "./apps/minio/minio"
  namespace = "minio"
  ingress = "minio.happylittlecloud.ca"
  nfs_ip = "192.168.0.239"
  nfs_path = "/volume1/minio"
  access_key = var.minio_access_key
  secret_key = var.minio_secret_key
}


resource "kubernetes_namespace" "datasette_namespace" {
  metadata {
    name = "datasette"
  }
}

module "music" {
  source = "./apps/datasette/music"
  namespace = "datasette"
  ingress = "datasette.happylittlecloud.ca"
  s3_endpoint = "http://minio.minio.svc.cluster.local"
  s3_path = "www/datasette"
}
