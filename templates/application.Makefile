ifeq ($(wildcard chart/values.yaml),)
	IMAGE_NAME =
else
	IMAGE_NAME = $(shell yq '.image.image' chart/values.yaml)
endif

DOCKER_ARGS := buildx build --platform linux/amd64,linux/arm64,linux/arm/v7

build: Dockerfile
	docker $(DOCKER_ARGS) .

push: build
ifndef IMAGE_NAME
	$(error There is no image name)
endif
	docker $(DOCKER_ARGS) . -t $(IMAGE_NAME) --push
